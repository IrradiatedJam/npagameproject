﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Playground/Gameplay/Music Player")]
public class MusicPlayer : MonoBehaviour
{
    public AudioClip musicToPlay;

    public static AudioSource musicSource = null;

    // Start is called before the first frame update
    void Start()
    {
        if (musicSource == null)
        {
            GameObject newObject = new GameObject();
            DontDestroyOnLoad(newObject);
            musicSource = newObject.AddComponent<AudioSource>();
            musicSource.playOnAwake = false;
            musicSource.loop = true;
        }

        if (musicToPlay != null)
            PlayMusic(musicToPlay);
    }

    public void PlayMusic(AudioClip _musicToPlay)
    {
        StartCoroutine(PlayMusic_CR(_musicToPlay));
    }


    private IEnumerator PlayMusic_CR(AudioClip _musicToPlay)
    {
        if (musicSource.clip == _musicToPlay)
        {
            yield break;
        }

        float fadeTime = 1f; // how long it will take to fade out
        if (musicSource.isPlaying)
        {
            while (musicSource.volume > 0)
            {
                musicSource.volume -= Time.deltaTime / fadeTime;
                yield return null;
            }
            musicSource.clip = _musicToPlay;
            musicSource.volume = 1f;
        }
        
        musicSource.clip = _musicToPlay;
        musicSource.volume = 1f;
        musicSource.Play();
        
        yield break;
    }

}
